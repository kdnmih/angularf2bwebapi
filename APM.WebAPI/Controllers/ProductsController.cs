﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.OData;
using APM.WebAPI.Models;
using APM.WebAPI.Repository;

namespace APM.WebAPI.Controllers
{
    [EnableCors("http://localhost:10180", "*", "*")]
    public class ProductsController : ApiController
    {
        private readonly IProductRepository repository;

        public ProductsController(IProductRepository repository)
        {
            this.repository = repository;
        }

        // GET: api/Products
        [EnableQuery()]
        [ResponseType(typeof(Product))]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(repository.Retrieve().AsQueryable());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [ResponseType(typeof(Product))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Product product;
                if (id > 0)
                {
                    product = repository.Retrieve().FirstOrDefault(p => p.ProductId == id);
                    if (product == null) { return NotFound(); }
                }
                else
                {
                    product = repository.Create();
                }

                return Ok(product);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult Post([FromBody]Product product)
        {
            try
            {
                if (product == null) { return BadRequest("Product cannot be null"); }
                if (!ModelState.IsValid) { return BadRequest(ModelState); }

                Product newProduct = repository.Save(product);
                if (product == null)
                {
                    return Conflict();
                }

                return Created<Product>(Request.RequestUri + newProduct.ProductId.ToString(), newProduct);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Products/5
        public IHttpActionResult Put(int id, [FromBody]Product product)
        {
            try
            {
                if (product == null) { return BadRequest("Product cannot be null"); }
                if (!ModelState.IsValid) { return BadRequest(ModelState); }

                Product updatedProduct = repository.Save(id, product);
                if (updatedProduct == null) { return NotFound(); }
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
        }
    }
}