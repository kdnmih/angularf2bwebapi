using System.Collections.Generic;
using APM.WebAPI.Models;

namespace APM.WebAPI.Repository
{
    public interface IProductRepository
    {
        Product Create();
        List<Product> Retrieve();
        Product Save(Product product);
        Product Save(int id, Product product);
    }
}